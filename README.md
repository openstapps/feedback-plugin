# @openstapps/feedback-plugin

## Prerequisites

* `node` and `npm` installed
* a backend, which is running locally (e.g. on http://localhost:3000)

## How to get started

To install all required `npm` packages:

```shell script
npm install
```

To prepare the script to be executed, run:

```shell script
npm run build
```

To start the plugin, you need to execute the code in "CLI" script:

```shell script
node lib/cli.js -b <backendURL> -n <pluginName> -r <routeName> -u <URL> -p <port> -m <mailHost> -a <mailPort> -t <mailTo>, -l <localHostname>

With fallback values being:
backendURL = http://localhost:3000 , pluginName = feedback-plugin , 
routeName = feedback , URL = http://localhost, port = 4001 ,
mailHost = localhost , mailPort = 25 , mailTo = root@localhost , localHostname = undefined
```

Using environment variables you can set backendURL via `STAPPS_BACKEND`, URL via `PLUGIN_URL` and port via `PLUGIN_URL`.

Using the (feedback plugin specific) environment variables it is **highly recommended** to set:

- `FEEDBACK_PLUGIN_MAIL_HOST` for mailHost
- `FEEDBACK_PLUGIN_MAIL_PORT` for mailPort
- `FEEDBACK_PLUGIN_MAIL_TO` for mailTo
- `FEEDBACK_PLUGIN_LOCAL_HOSTNAME` for localHostname
