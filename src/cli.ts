/*
 * Copyright (C) 2021-2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {HttpClient} from '@openstapps/api/lib/http-client';
import {PluginClient} from '@openstapps/api/lib/plugin-client';
import {SCFeedbackRoute} from '@openstapps/core';
import {Converter} from '@openstapps/core-tools/lib/schema';
import {Logger} from '@openstapps/logger';
import {Command, Option} from 'commander';
import {readFileSync} from 'fs';
import path from 'path';
import {createMailConfig} from './common';
import {FeedbackPlugin} from './plugin/feedback-plugin';

process.on('unhandledRejection', error => {
  throw error;
});

/* eslint-disable unicorn/prefer-module */
const pluginVersion = JSON.parse(readFileSync(path.join(__dirname, '..', 'package.json')).toString()).version;

const program = new Command()
  .version(pluginVersion)
  .addOption(
    new Option('-b, --backend-url <string>', 'URL of the StApps backend deployment')
      .default('http://localhost:3000')
      .env('STAPPS_BACKEND'),
  )
  .addOption(new Option('-n, --plugin-name <string>', 'The name of the plugin').default('feedback-plugin'))
  .addOption(
    new Option('-r, --route-name <string>', 'The name of the route').default(
      new SCFeedbackRoute().urlPath.replace('/', ''),
    ),
  )
  .addOption(
    new Option('-u, --url <string>', 'The url of the plugin').default('http://localhost').env('PLUGIN_URL'),
  )
  .addOption(new Option('-p, --port <number>', 'The port of the plugin').default('4001').env('PLUGIN_PORT'))
  .addOption(
    new Option('-m, --mail-host <string>', 'Host of the mail server')
      .default('localhost')
      .env('FEEDBACK_PLUGIN_MAIL_HOST'),
  )
  .addOption(
    new Option('-a, --mail-port <number>', 'Port of the mail server')
      .default('25')
      .env('FEEDBACK_PLUGIN_MAIL_PORT'),
  )
  .addOption(
    new Option('-t, --mail-to <string>', 'Mail address for receiving mails')
      .default('root@localhost')
      .env('FEEDBACK_PLUGIN_MAIL_TO'),
  )
  .addOption(
    new Option('-l, --local-hostname <string>', 'Hostname of local device').env(
      'FEEDBACK_PLUGIN_LOCAL_HOSTNAME',
    ),
  );

program.parse();
const options = program.opts();

// create an instance of the PluginClient
const pluginClient = new PluginClient(new HttpClient(), options.backendUrl);

// create an instance of your plugin
const plugin = new FeedbackPlugin(
  Number.parseInt(options.port, 10),
  options.pluginName,
  options.url,
  `/${options.routeName}`,
  options.backendUrl,
  new Converter(path.resolve(__dirname, '..', 'src', 'plugin', 'protocol')), // an instance of the converter. Required
  // because your requests and response schemas are defined in the plugin. The path should lead to your request and
  // response interfaces
  'SCFeedbackRequest',
  'SCFeedbackResponse',
  JSON.parse(readFileSync(path.resolve(__dirname, '..', 'package.json')).toString()).version, // get the version of the plugin from the package.json
  createMailConfig(
    options.mailHost,
    Number.parseInt(options.mailPort, 10),
    options.mailTo,
    undefined,
    options.localHostname,
  ),
);

pluginClient
  .registerPlugin(plugin)
  .then(() => {
    Logger.ok(`Successfully registered plugin '${options.pluginName}' on /${options.routeName} .`);
  })
  // eslint-disable-next-line unicorn/prefer-top-level-await
  .catch((error: Error) => {
    throw error;
  });

for (const signal of [`exit`, `SIGINT`, `SIGUSR1`, `SIGUSR2`, `SIGTERM`]) {
  process.once(signal as NodeJS.Signals, () => {
    pluginClient
      .unregisterPlugin(plugin)
      .then(() => {
        Logger.ok(`Successfully unregistered plugin '${options.pluginName}' from /${options.routeName} .`);
      })
      .catch((error: Error) => {
        throw error;
      });
  });
}
