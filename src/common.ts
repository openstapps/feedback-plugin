/*
 * Copyright (C) 2021-2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {MailConfig} from './plugin/mail-config';

/**
 * Creates mail config from the provided parameters
 *
 * @param host Host of used mail server
 * @param port Port to use on the mail server
 * @param toMail Where to receive feedback emails
 * @param fromMail Default email in case of unknown email of the sender
 */
export function createMailConfig(
  host: string,
  port: number,
  toMail: string,
  fromMail?: string,
  localHostname?: string,
) {
  const mailConfig: MailConfig = {
    to: {
      name: 'App Support',
      address: toMail,
    },
    host: host,
    port: port,
    subjectPrefix: 'Open StApps App Feedback',
    defaultFrom: {
      name: 'Anonym',
      address: fromMail ?? toMail,
    },
    attachmentName: 'context.json',
    localHostname: localHostname,
  };

  return mailConfig;
}
