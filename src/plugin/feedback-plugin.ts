/*
 * Copyright (C) 2021-2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Plugin} from '@openstapps/api/lib/plugin';
import {SCFeedbackRoute} from '@openstapps/core';
import {Converter} from '@openstapps/core-tools/lib/schema';
import {Logger} from '@openstapps/logger';
import {Request, Response} from 'express';
import {StatusCodes} from 'http-status-codes';
import {createTransport, Transporter} from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import {MailConfig} from './mail-config';
import {SCFeedbackRequest} from './protocol/request';
import {SCFeedbackResponse} from './protocol/response';

/**
 * Plugin handling the feedback requests
 */
export class FeedbackPlugin extends Plugin {
  /**
   * Transport to use for sending emails
   */
  private readonly transport: Transporter<SMTPTransport.SentMessageInfo>;

  constructor(
    port: number,
    public name: string,
    public url: string,
    public route: string,
    protected backendUrl: string,
    converter: Converter,
    requestName: string,
    responseName: string,
    version: string,
    private readonly mailConfig: MailConfig,
  ) {
    super(port, name, url, route, backendUrl, converter, requestName, responseName, version);
    this.transport = createTransport({
      host: mailConfig.host,
      port: mailConfig.port,
      name: mailConfig.localHostname,
    });
  }

  /**
   * The method that gets called when its route is being invoked
   *
   * @param request the express request object
   * @param response the express response object
   */
  protected async onRouteInvoke(request: Request, response: Response): Promise<void> {
    // get the body from the request as a SCFeedbackRequest for static type-safety
    const requestBody = request.body as SCFeedbackRequest;

    const sender = requestBody?.authors?.[0];
    const text = requestBody.messageBody;

    try {
      const smtpResponse = await this.transport.sendMail({
        to: {
          address: this.mailConfig.to.address,
          name: this.mailConfig.to.name,
        },
        envelope: {
          from: this.mailConfig.to.address,
          to: this.mailConfig.to.address,
        },
        from: {
          address: sender?.email ?? this.mailConfig.defaultFrom.address,
          name: sender?.name ?? this.mailConfig.defaultFrom.name,
        },
        subject: `${this.mailConfig.subjectPrefix}: ${requestBody.name}`,
        text: text,
        attachments: [
          {
            filename: this.mailConfig.attachmentName,
            content: JSON.stringify(requestBody.metaData),
            contentType: 'application/json',
          },
        ],
      });
      if (smtpResponse?.rejected?.length > 0) {
        throw new Error(`Failed to deliver feedback mail:\n\n${JSON.stringify(smtpResponse, undefined, 2)}`);
      }

      // send the response
      response.status(new SCFeedbackRoute().statusCodeSuccess).json({} as SCFeedbackResponse);
    } catch (error) {
      void Logger.error(error);
      // send the error response
      response.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
    }
  }
}
