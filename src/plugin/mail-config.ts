/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Address} from 'nodemailer/lib/mailer';

/**
 * Defines configuration parameters for the feedback plugin
 */
export interface MailConfig {
  /**
   * Complete name of the attachment file (with extension if wanted)
   */
  attachmentName: string;
  /**
   * A fallback when sender information is not available
   */
  defaultFrom: Address;
  /**
   * Host address for sending emails, e.g. mail.server.uni-frankfurt.de
   */
  host: string;
  /**
   * Name/Domain of the host running this script
   */
  localHostname?: string;
  /**
   * Port for sending emails
   */
  port: number;
  /**
   * Starting text of the sent emails
   */
  subjectPrefix: string;
  /**
   * Where to send feedback emails
   */
  to: Address;
}
