FROM registry.gitlab.com/openstapps/projectmanagement/node

USER root
WORKDIR /app
ENTRYPOINT ["node", "lib/cli.js"]

ADD . /app
RUN npm ci

USER node
