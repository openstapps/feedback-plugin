/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {createSandbox, SinonSpy} from 'sinon';
import {createMailConfig} from '../lib/common';
import {FeedbackPlugin} from '../lib/plugin/feedback-plugin';
import {resolve} from 'path';
import {Converter} from '@openstapps/core-tools/lib/schema';
import {readFileSync} from 'fs';
import {expect} from 'chai';
import {Request, Response} from 'express';
import {mockReq, mockRes} from 'sinon-express-mock';
import {MailOptions} from 'nodemailer/lib/smtp-transport';
import {mailConfig, options} from './data';
import {SCFeedbackRequest, SCThingOriginType, SCThingType} from '@openstapps/core';
import Mail from 'nodemailer/lib/mailer';
import {StatusCodes} from 'http-status-codes';

class TestFeedbackPlugin extends FeedbackPlugin {
  public async invokeRoute(req: Request, res: Response) {
    await super.onRouteInvoke(req, res);
  }
}

describe('Feedback Plugin', async function () {
  // increase timeout for the suite
  this.timeout(10000);

  const sandbox = createSandbox();
  let testFeedbackPlugin: TestFeedbackPlugin;
  let mailOptions: MailOptions;
  let sendMailSpy: SinonSpy;
  let mockedRequest: any;
  let mockedResponse: any;

  const requestBody: SCFeedbackRequest = {
    audiences: [],
    categories: [],
    categorySpecificValues: undefined,
    messageBody: 'Foo Message',
    metaData: {
      platform: 'Foo OS',
      scope: undefined,
      state: undefined,
      userAgent: 'Bar Browser',
      version: '1.23',
    },
    name: 'Foo Title',
    origin: {
      type: SCThingOriginType.User,
      created: '2020-09-11T12:30:00Z',
    },
    type: SCThingType.Message,
    uid: 'foo-uid-123',
  };
  const request = {
    body: requestBody,
  };
  const response = {
    status: () => {
      return {
        json: (_data: any) => {},
      };
    },
  };

  before(async () => {
    sandbox.stub(Converter.prototype, 'getSchema');
    sendMailSpy = sandbox.stub(Mail.prototype, 'sendMail').callsFake(async options => {
      mailOptions = options;
    });
    mockedRequest = mockReq(request);
    mockedResponse = mockRes(response);
    // create an instance of your plugin
    testFeedbackPlugin = new TestFeedbackPlugin(
      Number.parseInt(options.port, 10),
      options.pluginName,
      options.url,
      `/${options.routeName}`,
      options.backendUrl,
      new Converter(resolve(__dirname, '..', 'src', 'plugin', 'protocol')), // an instance of the converter. Required
      // because your requests and response schemas are defined in the plugin. The path should lead to your request and
      // response interfaces
      'SCFooFeedbackRequest',
      'SCFooFeedbackResponse',
      JSON.parse(readFileSync(resolve(__dirname, '..', 'package.json')).toString()).version, // get the version of the plugin from the package.json
      createMailConfig(options.mailHost, Number.parseInt(options.mailPort, 10), options.mailTo),
    );
  });

  after(async () => {
    await testFeedbackPlugin.close();
    sandbox.restore();
  });

  it('should send mail on route invoke', async () => {
    await testFeedbackPlugin.invokeRoute(mockedRequest, mockedResponse);

    expect(sendMailSpy.calledOnce).to.be.true;
  });

  it('should send mail with proper options', async () => {
    const expectedMailOptions: MailOptions = {
      to: {
        address: mailConfig.to.address,
        name: mailConfig.to.name,
      },
      envelope: {
        from: mailConfig.to.address,
        to: mailConfig.to.address,
      },
      from: {
        address: mailConfig.defaultFrom.address,
        name: mailConfig.defaultFrom.name,
      },
      subject: `${mailConfig.subjectPrefix}: ${request.body.name}`,
      text: `${request.body.messageBody}`,
      attachments: [
        {
          filename: mailConfig.attachmentName,
          content: JSON.stringify(requestBody.metaData),
          contentType: 'application/json',
        },
      ],
    };
    await testFeedbackPlugin.invokeRoute(mockedRequest, mockedResponse);

    expect(mailOptions).to.be.eql(expectedMailOptions);
  });

  it('should response with status 200 and empty object when no errors', async () => {
    let gotStatus: any;
    let gotJson: any;
    const response = {
      status: (status: any) => {
        gotStatus = status;
        return {
          json: (data: any) => {
            gotJson = data;
          },
        };
      },
    };
    const mockedReq = mockReq({body: {}});
    const mockedRes = mockRes(response);
    await testFeedbackPlugin.invokeRoute(mockedReq, mockedRes);

    expect(gotStatus).to.be.eql(StatusCodes.OK);
    expect(gotJson).to.be.eql({});
  });

  it('should response with internal server error when errors', async () => {
    sandbox.restore();
    sandbox.stub(Mail.prototype, 'sendMail').throws(new Error('Foo Error'));
    let gotStatus: any;
    const response = {
      sendStatus: (status: any) => {
        gotStatus = status;
      },
    };
    const mockedReq = mockReq({body: {}});
    const mockedRes = mockRes(response);
    await testFeedbackPlugin.invokeRoute(mockedReq, mockedRes);

    expect(gotStatus).to.be.eql(StatusCodes.INTERNAL_SERVER_ERROR);
  });
});
