/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {createMailConfig} from '../lib/common';
import {mailConfig, options} from './data';

describe('createMailConfig', () => {
  it('should create mail config without from mail', () => {
    expect(createMailConfig(options.mailHost, options.mailPort, options.mailTo)).to.eql(mailConfig);
  });

  it('should create mail config with from mail', () => {
    const expectedConfig = {... mailConfig, defaultFrom: {...mailConfig.defaultFrom, address: 'foo@foobar.com'}};

    expect(createMailConfig(options.mailHost, options.mailPort, options.mailTo, 'foo@foobar.com')).to.eql(expectedConfig);
  });
});
