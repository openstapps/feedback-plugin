/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {OptionValues} from 'commander';
import {MailConfig} from '../lib/plugin/mail-config';

export const options: OptionValues = {
  pluginName: 'foo feedback plugin',
  url: 'http://foobar',
  routeName: 'foo',
  backendUrl: 'http://foobar:1234',
  port: 4321,
  mailHost: 'mail.foobar.com',
  mailPort: 12,
  mailTo: 'app@foobar.com'
}

export const mailConfig: MailConfig = {
  to: {
    name: 'App Support',
    address: options.mailTo,
  },
  host: options.mailHost,
  port: options.mailPort,
  subjectPrefix: 'Open StApps App Feedback',
  defaultFrom: {
    name: 'Anonym',
    address: options.mailTo,
  },
  attachmentName: 'context.json',
  localHostname: undefined,
};
