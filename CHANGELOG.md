# [1.5.0](https://gitlab.com/openstapps/feedback-plugin/compare/v1.4.0...v1.5.0) (2022-08-24)



# [1.4.0](https://gitlab.com/openstapps/feedback-plugin/compare/v1.3.0...v1.4.0) (2021-12-20)



# [1.3.0](https://gitlab.com/openstapps/feedback-plugin/compare/v1.2.0...v1.3.0) (2021-10-20)


### Bug Fixes

* NO_CONTENT does not provide valid empty object ([3749cb1](https://gitlab.com/openstapps/feedback-plugin/commit/3749cb179189c4c98984844f983dc7175f115114))



# [1.2.0](https://gitlab.com/openstapps/feedback-plugin/compare/v1.1.0...v1.2.0) (2021-10-04)


### Features

* unregister plugin on exit ([ef4c3c8](https://gitlab.com/openstapps/feedback-plugin/commit/ef4c3c8c1c166fc8ac1949e19f14f100ed97eb8b))



# [1.1.0](https://gitlab.com/openstapps/feedback-plugin/compare/v1.0.1...v1.1.0) (2021-10-04)


### Features

* add smtp hostname option ([28834da](https://gitlab.com/openstapps/feedback-plugin/commit/28834dada1c170f74d40feaa3dfe0db4cad31167))



## [1.0.1](https://gitlab.com/openstapps/feedback-plugin/compare/v1.0.0...v1.0.1) (2021-10-04)


### Bug Fixes

* adjust smtp envelope and other options ([2a4cd85](https://gitlab.com/openstapps/feedback-plugin/commit/2a4cd857683eb6a5bde11d104de93807882feb5b))



# [1.0.0](https://gitlab.com/openstapps/feedback-plugin/compare/629af05c51b72dd54788b29c3dbfee9cbaa2dcc2...v1.0.0) (2021-09-24)


### Features

* implement feedback logic ([ea767f0](https://gitlab.com/openstapps/feedback-plugin/commit/ea767f0816a7f3c6392f2959f7df0149ae26fb96))
* initial commit (minimal-connector) ([629af05](https://gitlab.com/openstapps/feedback-plugin/commit/629af05c51b72dd54788b29c3dbfee9cbaa2dcc2))



